package com.example.authentication

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.util.Log.d
import android.view.View
import android.widget.Toast
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.ktx.auth
import com.google.firebase.ktx.Firebase
import kotlinx.android.synthetic.main.activity_sign_up.*

class SignUpActivity : AppCompatActivity() {
    private lateinit var auth: FirebaseAuth
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_sign_up)
        init()
    }
    private fun init(){
        auth = Firebase.auth
        signUpButton2.setOnClickListener {
           singUp()
        }
    }
    private fun singUp(){
        val email=emailField.text.toString()
        val password=passwordField.text.toString()
        val repeatPassword=password2Field.text.toString()
        if(email.isNotEmpty()&&password.isNotEmpty()&&repeatPassword.isNotEmpty()){
            if(password==repeatPassword){
                progressBar.visibility=View.VISIBLE
                signUpButton2.isClickable=false
                auth.createUserWithEmailAndPassword(email, password)
                    .addOnCompleteListener(this) { task ->
                        progressBar.visibility=View.GONE
                        signUpButton2.isClickable=true
                        if (task.isSuccessful) {
                            // Sign in success, update UI with the signed-in user's information
                            d("", "createUserWithEmail:success")
                            val user = auth.currentUser
                        } else {
                            // If sign in fails, display a message to the user.
                            d("signUp", "createUserWithEmail:failure", task.exception)
                            Toast.makeText(baseContext, "Authentication failed.",
                                Toast.LENGTH_SHORT).show()
                        }

                        // ...
                    }
            }
            else{
                Toast.makeText(this,"please enter the correct passwords",Toast.LENGTH_SHORT).show()
            }
        }
        else{
            Toast.makeText(this,"Please fill all fields",Toast.LENGTH_SHORT).show()
        }

    }
}